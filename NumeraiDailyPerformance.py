import numerapi
import matplotlib.pyplot as plt
import pandas as pd
from tqdm import tqdm
import multiprocessing

napi = numerapi.NumerAPI()

my_username = 'mendrinos'
round_no = 224

df = pd.io.json.json_normalize(napi.daily_submissions_performances(my_username), sep="-")

leaderboard = napi.get_leaderboard(limit=2000)

users = []
for user_data in leaderboard:
    users.append(user_data['username'])

round_data = df.loc[df.roundNumber == round_no, :]
round_data.dropna(inplace=True)
current_data = round_data.iloc[-1]
my_corr = current_data['correlation']
my_mmc = current_data['mmc']


def getUserData(user):
    try:
        df = pd.io.json.json_normalize(napi.daily_submissions_performances(user), sep="-")
    except:
        return -1,-1
    
    
    round_data = df.loc[df.roundNumber == round_no, :]
    round_data.dropna(inplace=True)
    if not round_data.empty:
        current_data = round_data.iloc[-1]
        
        user_corr = current_data['correlation']
        user_mmc = current_data['mmc']
        
        return user_corr, user_mmc
    return -1, -1


if __name__ == '__main__':
    with multiprocessing.Pool(8) as pool:
        data = list(pool.map(getUserData, tqdm(users)))
        pool.close()
        pool.join()

    corrs = []
    mmcs = []
    
    for i in range(len(data)):
        corrs.append(data[i][0])
        mmcs.append(data[i][1])
    
    corrs.sort(reverse=True)
    mmcs.sort(reverse=True)
    
    my_corr_pos = corrs.index(my_corr)
    my_mmc_pos = mmcs.index(my_mmc)
    print("Correlation position today:", my_corr_pos, "out of", len(corrs))
    print("MMC position today:", my_mmc_pos, "out of", len(mmcs))
    
    corrs = list(filter(lambda a: a != -1, corrs))
    mmcs = list(filter(lambda a: a != -1, mmcs))
    
    fig, axs = plt.subplots(1, 2, sharey=True, tight_layout=True)
    plot_heights,bins,patches = axs[0].hist(corrs, bins=20, alpha=0.3)
    axs[0].plot([my_corr for i in range(400)], [i for i in range(400)], color='black', linewidth=2)
    axs[1].hist(mmcs, bins=20, alpha=0.3)
    axs[1].plot([my_mmc for i in range(400)], [i for i in range(400)], color='black', linewidth=2)
    